'use strict';

const axios = require('axios').default;

module.exports = function(Line) {
  Line.sendMessage = async (uuid, callback) => {
    var result;
    // TODO
    await axios.post('https://api.line.me/v2/bot/message/multicast', {
      to: [uuid],
      messages: [
        {
          type: 'text',
          text: 'Hello world',
        },
      ],
    }, {
      headers: {
        Authorization: 'Bearer UBGU+VajNC+ySjj+E1ytTltQ/d//xgxkn8taoG+hnCnISMR1xWPMPVcxh0FxZ8RL4aFOCUwOyUfYqkJcGFFHf9gTwPUJJvyroZnCAWOj5w3E+cRrZsyCkLErDK3YO0hPt+CL/FHVHp90h3iHA1u9cQdB04t89/1O/w1cDnyilFU=',
      },
    });
    return Promise.resolve({
      status: 'OK',
    });
  };
  Line.webhook = async function(body, callback) {
    console.log(body);
    const {
      events
    } = body;
    // TODO
    if (!events) {
      return Promise.resolve();
    }
    await events.map(async (event) => {
      const {
        replyToken,
      } = event;

      // process incoming message
      // read data from database
      // get some data
      await axios.post('https://api.line.me/v2/bot/message/reply', {
        replyToken: replyToken,
        messages: [
          {
            type: 'text',
            text: 'This is reply message',
          },
        ],
      }, {
        headers: {
          Authorization: 'Bearer UBGU+VajNC+ySjj+E1ytTltQ/d//xgxkn8taoG+hnCnISMR1xWPMPVcxh0FxZ8RL4aFOCUwOyUfYqkJcGFFHf9gTwPUJJvyroZnCAWOj5w3E+cRrZsyCkLErDK3YO0hPt+CL/FHVHp90h3iHA1u9cQdB04t89/1O/w1cDnyilFU=',
        },
      });
      return Promise.resolve();
    });
    
    return Promise.resolve(body);
  };
};
