import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import liff from '@line/liff'
import axios from 'axios';
function App() {
  const [profile, setprofile] = useState()
  useEffect(() => {
    initLine()
  }, [])
  const initLine = async () => {
    try {
      await liff.init({
        liffId: '1657261034-WQVzdG7R'
      })
      await liff.ready
      if (liff.isLoggedIn()) {
        const profile = await liff.getProfile()
        setprofile(profile)
      } else {
        liff.login()
        return
      }
    } catch (e) {
      console.log('Error occur', e)
    }
  }
  const handleSendMessage = async () => {
    try {
      await axios.post('http://localhost:3001/api/Lines/sendMessage', {
        uuid: profile.userId
      })
    } catch (e) {
      console.log('Error occur while send message', e)
    }
  }
  return (
    <div>
      Hello
      {JSON.stringify(profile)}
      <button onClick={handleSendMessage}>Send message</button>
    </div>
  );
}

export default App;
